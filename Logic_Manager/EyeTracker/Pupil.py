import cv2
import numpy


class Pupil(object):  # class inherits from object...
    # the perks if it listed here: https://stackoverflow.com/questions/4015417/python-class-inherits-object
    """
    class contains a pupil data,
    x,y - integers, representing the location of the pupil in the frame
    threshold - integer, representing the level of threshold used to get the most accurate data
    frame - contains a frame of the eye
    """
    def __init__(self, eye_frame, threshold):
        """
        constructor...
        :param eye_frame: frame of the eye
        :param threshold: the recommended threshold value for the eye
        """
        self.x = 0  # expresses the location of the pupil in the frame
        self.y = 0
        self.frame = eye_frame
        self.threshold = threshold
        self.find_iris(eye_frame)

    @staticmethod
    def manipulate_frame(eye_frame, threshold):
        """
        function manipulates the frame so the object detection will be more accurate
        :param eye_frame: frame that contains the eye
        :param threshold: threshold level to manipulate the frame by
        :return: the input frame, after some manipulations
        firstly, using bilateral filtering to preserve sharp edges in the frame to make the detection easier
        secondly, using erosion to reduce the amount of shapes in the frame to make the detection easier
        thirdly, using threshold to separate the darker iris from the rest of the eye
        after the manipulation, the frame will contain a black circle (iris) and white background
        """
        kernel = numpy.ones((3, 3), numpy.uint8)
        new_frame = cv2.bilateralFilter(eye_frame, 10, 15, 15)
        new_frame = cv2.erode(new_frame, kernel, iterations=3)
        new_frame = cv2.threshold(new_frame, threshold, 255, cv2.THRESH_BINARY)[1]

        return new_frame

    def find_iris(self, eye_frame):
        """
        function finds the iris in the input frame, and sets the self object values accordingly
        :param eye_frame: frame that contains the eye
        :return:none, function edit the self object
        """
        self.frame = self.manipulate_frame(eye_frame, self.threshold)
        contours, _ = cv2.findContours(self.frame, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        # finding objects that are darker (iris)
        contours = sorted(contours, key=cv2.contourArea)
        # assuming that the manipulation worked as it should, the contours should be a list of two objects
        # the smaller one is the iris and the bigger one is the background
        try:
            moments = cv2.moments(contours[0]) # taking the smaller object (iris)
            # assuming that the smallest contour is the pupil, and the second smallest is the iris
            # calculating the centroid of the blob, by the formula:
            self.x = int(moments['m10'] / moments['m00'])  # the pupil is always located at the center of the iris
            self.y = int(moments['m01'] / moments['m00'])  # therefore, we can locate the pupil by the iris
        except Exception: # this exception handles the case that the program couldn't find the iris
            print("Couldn't find iris!")