import Pupil
import math
import cv2
import numpy

RIGHT_EYE = 0
LEFT_EYE = 1
# the 68 multi pie points represents a point on a generic face I.E: 9 is the center of the chin
# after using the shape predictor to get those points we use this list to index them
LEFT_EYE_MULTI_PIE_POINTS = [37, 38, 39 ,40, 41, 42]
RIGHT_EYE_MULTI_PIE_POINTS = [43, 44, 45 ,46, 47, 48]


class Eye(object):

    def __init__(self, frame, face_landmarks, eye_side, helper):
        """
        constructor
        :param frame: a frame of the eye
        :param face_landmarks: landmarks of the face found by the shape predictor
        :param eye_side: right eye or left eye
        :param helper: class that
        """
        self.frame = None
        self.pupil = None
        self.side = eye_side
        self.center_point = None
        self.crop_point = None
        self.blinking_value = None
        self.reinitiate(frame, face_landmarks, eye_side, helper)

    @staticmethod
    def get_center_point(pA, pB):
        """
        function returns the center point of the two input points
        :param pA: first point
        :param pB: second point
        :return: the center point
        """
        if type(pA) == tuple:
            return int((pA[0] + pB[0]) / 2), int((pA[1] + pB[1]) / 2)
        else:
            return int((pA.x + pB.x) / 2), int((pA.y + pB.y) / 2)

    @staticmethod
    def get_distance_between_points(pA, pB):
        """
        function calculates the distance between two input points
        :param pA: first points
        :param pB: second point
        :return: distance between those points
        """
        return math.sqrt(math.pow(abs(pA[0] - pB[0]), 2) + pow(abs(pA[1] - pB[1]), 2))

    def blinking_indicator(self, face_landmarks, multi_pie_points):
        """
        function calculates the ratio of the eye's width and the eye's height
        :param face_landmarks: points of the eye's landmarks
        :param multi_pie_points: landmarks' indexes
        :return: ratio: (width / height)
        """
        left_point = (face_landmarks.part(multi_pie_points[0]).x, face_landmarks.part(multi_pie_points[0]).y)  # getting points
        right_point = (face_landmarks.part(multi_pie_points[3]).x, face_landmarks.part(multi_pie_points[3]).y)
        top_point = self.get_center_point(face_landmarks.part(multi_pie_points[1]), face_landmarks.part(multi_pie_points[2]))
        bottom_point = self.get_center_point(face_landmarks.part(multi_pie_points[4]), face_landmarks.part(multi_pie_points[5]))
        height = self.get_distance_between_points(top_point, bottom_point)  # getting distance
        width = self.get_distance_between_points(left_point, right_point)
        try:
            return width / height
        except ZeroDivisionError:
            return 0

    def crop_frame_to_eye(self, frame, face_landmarks, multi_pie_points):
        """
        function gets a full face frame, and crops it to the eye only
        :param frame: face frame
        :param face_landmarks: eye landmarks (represents points in the frame which are the eyes)
        :param multi_pie_points: a list of points that are relevant for the specific eye (right / left)
        :return: none, function edits the self object
        """
        face_points = numpy.array([(face_landmarks.part(point).x, face_landmarks.part(point).y) for point in multi_pie_points])
        # the line above creates an array that contains the actual points (x, y) in the frame of the eye in the same
        # order as the points of the 68 multi pie were, that way we can choose a point on the generic diagram and get
        # the specific coordinates
        face_points = face_points.astype(numpy.int32) # casting to int
        x1 = face_points[1][0] + 25  # getting the diagonal points ( 1 = top left, 4 = bottom right)
        y1 = face_points[1][1] - 10  # adding some space to get the whole eye
        x2 = face_points[4][0] - 25
        y2 = face_points[4][1] + 10
        self.frame = frame[y1:y2, x2:x1]  # cutting the frame
        self.center_point = Eye.get_center_point((x2, y1), (x1, y2))  # setting the center point of the eye
        self.crop_point = (x2, y1)  # saving the point where we cut the frame for viewing (used only for 1st sprint)
        # because we need the program to work on its own...

    def reinitiate(self, face_frame, face_landmarks, eye_side, helper):
        """
        function refreshes all of the object's values according to the new input
        :param face_frame: frame of the face
        :param face_landmarks: landmarks found by the shape predictor (machine learned)
        :param eye_side: left eye / right eye
        :param helper: object that helps determine threshold and find iris
        :return: none, function edits the self object
        """
        if eye_side == RIGHT_EYE: # each eye has a different point in the 68 multi pie
            multi_pie_points = RIGHT_EYE_MULTI_PIE_POINTS
        else:
            multi_pie_points = LEFT_EYE_MULTI_PIE_POINTS

        self.blinking_value = self.blinking_indicator(face_landmarks, multi_pie_points)
        # blinking value contains a number that represent if the eye is closed

        self.crop_frame_to_eye(face_frame, face_landmarks, multi_pie_points)

        self.pupil = Pupil.Pupil(self.frame, helper.threshold_tester(self.frame))


