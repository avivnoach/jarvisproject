import cv2
import os
import dlib
import Eye
import Helper


RIGHT_EYE = 0
LEFT_EYE = 1


class EyeTracker(object):

    def __init__(self):
        """
        constructor
        """
        self.frame = None
        self.Xnose = None
        self.Ynose = None
        self.left_eye = None
        self.right_eye = None
        self.horizontal_middle_value = None
        self.vertical_middle_value = None
        self.value_calibrator = Helper.Helper()  # the class will have an object that calibrates threshold, etc
        self.facial_detector = dlib.get_frontal_face_detector()  # object that detect faces
        model_file_path = os.path.abspath(os.path.dirname(__file__))
        model_file_path = os.path.abspath(os.path.join(model_file_path, "trainedML/shape_predictor_68_face_landmarks.dat"))
        self.shape_predictor = dlib.shape_predictor(model_file_path)  # object that find shapes according to ML file

    def refresh(self, frame):
        """
        function refreshes the eye tracker object fields according to the new input frame
        :param frame: the new frame of the face
        :return: none, the function edits the object's fields
        """
        frame = cv2.flip(frame, 1)
        gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        self.frame = frame
        faces = self.facial_detector(gray_frame)  # getting the faces in the frame
        if(len(faces) == 1):
            # getting the multi pie points on the face that found:
            face_landmarks = self.shape_predictor(gray_frame, faces[0])
            self.Xnose = face_landmarks.part(30).x
            self.Ynose = face_landmarks.part(30).y
            # setting eyes
            self.right_eye = Eye.Eye(gray_frame, face_landmarks, RIGHT_EYE, self.value_calibrator)
            self.left_eye = Eye.Eye(gray_frame, face_landmarks, LEFT_EYE, self.value_calibrator)

    def draw_info_on_frame(self):
        """
        function draws the location of the eyes (more specifically the pupil) on the object's frame
        :return: a new frame with a circle on the eyes
        """
        frame = self.frame.copy()
        color = (0, 0, 255)
        if self.pupils_found():
            x_left, y_left = self.get_eye_absolute_location(LEFT_EYE)
            x_right, y_right = self.get_eye_absolute_location(RIGHT_EYE)
            cv2.circle(frame, (x_right, y_right), 4, color)
            cv2.circle(frame, (x_left, y_left), 4, color)
            cv2.circle(frame, (self.left_eye.center_point), 4, (0,255,0))
            cv2.circle(frame, (self.right_eye.center_point), 4, (0,255,0))
            cv2.circle(frame, (self.Xnose, self.Ynose), 2, (0,255,0))
            font = cv2.FONT_HERSHEY_SIMPLEX
            bottomLeftCornerOfText = (10, 50)
            fontScale = 1
            fontColor = (255, 255, 255)
            lineType = 2

            cv2.putText(frame, self.determine_eye_direction()[0],
                        bottomLeftCornerOfText,
                        font,
                        fontScale,
                        color,
                        lineType)
        return frame

    def get_eye_absolute_location(self, eye_side):
        """
        function return the location  of the eye in the face frame (and not in the eye frame)
        to get the location of the eye in the big (face frame) we need to add to location where we cut image
        to the eye, and add to that the location of the pupil in the eye frame.
        :param eye_side: left eye / right eye
        :return: (X, y) of the location of the pupil in the object's frame
        """
        if eye_side == RIGHT_EYE:
            return (self.right_eye.pupil.x + self.right_eye.crop_point[0],
                    self.right_eye.pupil.y + self.right_eye.crop_point[1])
        else:
            return (self.left_eye.pupil.x + self.left_eye.crop_point[0],
                    self.left_eye.pupil.y + self.left_eye.crop_point[1])

    def pupils_found(self):
        """
        function checks if the program could find the pupils or not
        :return: true / false if the pupil were located or not
        """
        try:
            return self.right_eye.pupil != None or self.left_eye.pupil != None
        except (AttributeError): # sometimes the camera can't locate the pupil at the first frames
            print("Eyes were not located!")
            return False  
    def horizontal_indicator(self):
        """
        function calculates an indicator that representing the horizontal place of the pupil in the eye
        the function uses the pupil place, and the center of the eye
        :return: an indicator that goes between 0 and 1, 0 is all the way left, 1 is all the way right 0.5 is the middle
        """
        left_eye_indicator = self.left_eye.pupil.x / ((self.left_eye.center_point[0]-self.left_eye.crop_point[0])*2)
        right_eye_indicator = self.right_eye.pupil.x / ((self.right_eye.center_point[0]-self.right_eye.crop_point[0])*2)
        return (left_eye_indicator + right_eye_indicator) / 2

    def determine_eye_direction(self):
        horizontal_tolerance = 2
        if self.pupils_found():
            horizontal = self.horizontal_indicator()
            horizontal_str = ""
            vertical_str = ""
            focused = True

            nose_to_left_eye = self.get_x_distance(self.Xnose, self.left_eye.center_point[0])
            nose_to_right_eye = self.get_x_distance(self.Xnose, self.right_eye.center_point[0])

            if nose_to_right_eye / nose_to_left_eye > horizontal_tolerance:
                horizontal_str = "looking left"
                focused = False
            elif nose_to_left_eye / nose_to_right_eye > horizontal_tolerance:
                horizontal_str = "looking right"
                focused = False
            else:
                horizontal_str = "looking forward"
                focused = True
            return horizontal_str, focused


    def calibrate_person_eyes(self, webcam):
        vertical_value = 0
        horizontal_value = 0
        i = 0
        for i in range(10):
            self.refresh(webcam.read()[1])
            if self.pupils_found():
                horizontal_value += self.horizontal_indicator()

        self.horizontal_middle_value = horizontal_value / 10
        self.vertical_middle_value = vertical_value / 10
        print(self.horizontal_middle_value, self.vertical_middle_value)

    def get_x_distance(self, x1, x2):
        if abs(x1-x2) == 0:
                return 0.1
        else:
            return abs(x1-x2)