import cv2
import EyeTracker
import time
from Driver import Driver
from firebase import Firebase
from firebase_admin import credentials, initialize_app, storage
import os
import PIL
import face_recognition
from datetime import date, datetime
import facerecon as JarvisFaceRecognition
import DatabaseManager

def main():
    eye_tracker = EyeTracker.EyeTracker()  # creating tracking object
    webcam = cv2.VideoCapture(0)  # setting webcam
    eye_tracker.calibrate_person_eyes(webcam)  # calibrating values
    DatabaseManager.retrieve_images(DatabaseManager.get_drivers_list())  # getting known drivers images
    JarvisFaceRecognition.load_known_faces()  # loading the known faces
    driver = JarvisFaceRecognition.login()  # login using the face from the camera
    if driver:
        print("Welcome: " + driver)
    else:
        user_choice = input("driver not found!")


    global focused_value
    global unfocused_value
    focused_value = 0
    unfocused_value = 0


    while True:
        _, frame = webcam.read()  # getting frame
        eye_tracker.refresh(frame)  # refreshing data according to the frame

        direction, focused = eye_tracker.determine_eye_direction()  # getting results

        print(direction)

        if focused:  # updating values for percentage
            focused_value = focused_value + 1
        else:
            unfocused_value = unfocused_value + 1



        time.sleep(0.2) #doing this 5 times a second

        if cv2.waitKey(1) == 27:
            break

def update_results_on_database():
    print("Not implemented yet") # Amir: push results to DB



if __name__ == '__main__':
    main()