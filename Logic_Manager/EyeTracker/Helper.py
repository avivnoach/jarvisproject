import Pupil
import cv2

LEFT = 1
RIGHT = 0
NUMBER_OF_TRIALS = 20
COMPARABLE_IRIS_AREA = 0.275
MARGIN = 5



class Helper:


    def __init__(self):
        self.left_eye_trials = []
        self.right_eye_trials = []

    def get_threshold(self, side):
        # function returns the average recommended threshold from the trials checked
        if side == LEFT:
            return int(sum(self.left_eye_trials) / len(self.left_eye_trials))
        elif side == RIGHT:
            return int(sum(self.right_eye_trials) / len(self.right_eye_trials))
        else:
            return

    @staticmethod
    def threshold_tester(frame):
        """
        function calculates the optimal threshold value for the input frame
        the function has a value to compare to in order to get the optimal results,
        the function sets a different threshold values on an image and check in which image, the iris size is
        the closest to the comparable value, and the closest one is the best threshold value
        THE PARAMETER "COMPARABLE_IRIS_AREA" WAS FOUND BY TRYING A LOT OF TIMES UNTIL GETTING THE OPTIMAL RESULTS
        :param frame: a frame if the eye
        :return: the optimal threshold value
        """
        threshold_trials = {}
        recommended_threshold_value = 0
        recommended_threshold_result = 100

        for t in range(5, 100, 5):
            manipulated_frame = Pupil.Pupil.manipulate_frame(frame, t)
            threshold_trials[t] = Helper.calculate_iris_area(manipulated_frame)
        for threshold_val, threshold_result in threshold_trials.items():
            if abs(threshold_result - COMPARABLE_IRIS_AREA) < recommended_threshold_result:
                recommended_threshold_value = threshold_val
                recommended_threshold_result = abs(threshold_result - COMPARABLE_IRIS_AREA)
        return recommended_threshold_value

    @staticmethod
    def calculate_iris_area(frame):
        """
        function calculates the area that the iris takes on the frame (iris to background ratio)
        :param frame: a frame of the eye
        :return: iris to background ratio
        """
        # shrinking the frame a little bit, because when we cropped it we left it a little more space
        frame = frame[8: -8, 8:-8]
        height, width = frame.shape[:2]  # getting frame dimensions
        pixels = height * width  # calculating all the pixels
        black_pixels = pixels - cv2.countNonZero(frame)  # calculating black pixels
        try:
            return black_pixels / pixels  # returning ratio
        except ZeroDivisionError:
            print("Frame pixels are 0!")
            return None


