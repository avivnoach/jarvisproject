from objects.Driver import Driver
from firebase import Firebase
from firebase_admin import credentials, initialize_app, storage
import os
import PIL
import face_recognition
from datetime import date, datetime
import facerecon as JarvisFaceRecognition


KNOWN_FACES_DIR = 'knownDrivers'
UNKNOWN_FACES_DIR = 'unknownDrivers'


class DatabaseManager(object):
    def __init__(self):
        self.DRIVERS = []
        self.FIREBASE = None
        self.CAR_NUMBER = None
        self.configure()

    def configure(self):
        """
        function configures firebase and gets the car number from the car data file
        :return: firebase instance, car number
        """
        config = {
            "apiKey": "apiKey",
            "authDomain": "jarvis-proj-404.firebaseapp.com",
            "databaseURL": "https://jarvis-proj-404.firebaseio.com/",
            "storageBucket": "jarvis-proj-404.appspot.com"
        }
        program_data = open("PROGRAM_DATA.txt", "r").read()
        car_number = program_data.splitlines()[0].split(":")[-1]
        self.FIREBASE = Firebase(config)
        self.CAR_NUMBER = car_number


    def retrieve_images(self, users_list):
        """
        program saves the drivers' photos from the database into the computer, so the program could recognize them
        :param users_list: list of users to save pictures of
        :return: none
        """
        storage = self.FIREBASE.storage()
        for username in users_list:
            if not os.path.isfile('knownDrivers/' + username + '/' + username + '.jpg'):  # if the user is already in the pc
                if not os.path.isdir('knownDrivers/' + username):
                    os.mkdir('knownDrivers/' + username)
                storage.child('DriversPictures/' + username + '.jpeg').download('knownDrivers/' + username + '/' + username + '.jpg')
                self.rotate_image('knownDrivers/' + username + '/' + username + '.jpg')


    def get_drivers_list(self):
        """
        function gets a list of the users that are driving the car from the database
        :return: list of the users that are driving the car
        """
        database = self.FIREBASE.database()
        drivers = database.child("Cars").child(str(self.CAR_NUMBER)).child("drivers").get().each()
        if not os.path.isdir('knownDrivers'):
            os.mkdir('knownDrivers')
        if len(drivers) != self.fcount("knownDrivers"):
            for driver in drivers:
                self.DRIVERS.append(Driver(database.child("Users").child(driver.val()).child("name").get().val(),
                                      database.child("Users").child(driver.val()).child("email").get().val(),
                                      database.child("Users").child(driver.val()).child("username").get().val(),
                                      database.child("Users").child(driver.val()).child("uid").get().val()))
            self.save_drivers_locally(self.DRIVERS)
        #else get from file
        return database.child("Cars").child(str(self.CAR_NUMBER)).child("drivers").get().val()


    def load_drivers_from_file(self):
        with open("PROGRAM_DATA.txt", "r") as program_data:
            for line in program_data.read().splitlines()[1:]:
                line = line.split(",")
                self.DRIVERS.append(Driver(line[0], line[1], line[2], line[3]))



    def fcount(self, path, map = {}):
      count = 0
      for f in os.listdir(path):
        child = os.path.join(path, f)
        if os.path.isdir(child):
          child_count = self.fcount(child, map)
          count += child_count + 1 # unless include self
      map[path] = count
      return count


    def save_drivers_locally(self, drivers_list):
        with open("PROGRAM_DATA.txt", "w") as data_file:
            data_file.truncate()
            data_file.write("CARNUMBER:" + str(self.CAR_NUMBER) + '\n')
            for driver in drivers_list:
                data_file.write(driver.name + "," + driver.email + "," + driver.username + "," + driver.uid + "\n")


    def rotate_image(image_path):
        image = PIL.Image.open(image_path)
        width, height = image.size
        if width > height:
            out = image.rotate(90, expand=True)
            out.save(image_path)
            face = face_recognition.load_image_file(image_path)
            encoding = face_recognition.face_encodings(face)
            if len(encoding) == 0:
                image = PIL.Image.open(image_path)
                out = image.rotate(180, expand=True)
                out.save(image_path)


    def update_driving_history(self, username):
        """
        this function push to the driving_history branch in the firebase the date, time and the name of the driver who enter into the car
        :param username: the name of the driver who enter into the car
        :return: none
        """
        database = self.FIREBASE.database()
        date_to_push = date.today().strftime("%b-%d-%Y")
        time_to_push = datetime.now().strftime("%H:%M:%S")
        database.child("Cars").child(self.CAR_NUMBER).child("Driving_history").child(date_to_push).child(time_to_push).set(username)


    def push_unknown_drivers(self):
        """
        this function push a photo of the unknown driver
        :param file_name: the name of the photo file of the unknown driver
        :return: none
        """
        cred = credentials.Certificate("jarvis-proj-404-firebase-adminsdk-hyaie-a908b11d03.json")
        initialize_app(cred, {'storageBucket': 'jarvis-proj-404.appspot.com'})

        date_to_push = date.today().strftime("%b-%d-%Y")
        time_to_push = datetime.now().strftime("%H:%M:%S")
        picture_name = self.CAR_NUMBER + "&" + date_to_push + "&" + time_to_push

        bucket = storage.bucket()
        blob = bucket.blob("UnknownDrivers/" + str(self.CAR_NUMBER) + "/" + picture_name)
        blob.upload_from_filename(UNKNOWN_FACES_DIR + '/driver.png')

        self.update_driving_history("UNKNOWN_DRIVER")


    def push_focus_statistics(self, time_focus, time_unfocused, username):
        """
        this function push to specific user how much time he was focused in the driving and how much time he not
        :param time_focus: the time which the user was focused
        :param time_unfocused: the time which the user was unfocused
        :param username: the name of the user
        :return: none
        """
        database = self.FIREBASE.database()
        # get the old values in the firebase
        old_time_focus = database.child("Users").child(username).child("time_focus").get().val()
        old_time_unfocused = database.child("Users").child(username).child("time_unfocused").get().val()

        new_time_focus = time_focus
        new_time_unfocused = time_unfocused

        # if the values already exists adding the new values
        if old_time_focus is not None:
            new_time_focus += old_time_focus

        if old_time_unfocused is not None:
            new_time_unfocused += old_time_unfocused

        # push to firebase the values
        database.child("Users").child(username).child("insights").child("time_focus").set(new_time_focus)
        database.child("Users").child(username).child("insights").child("time_unfocused").set(new_time_unfocused)


