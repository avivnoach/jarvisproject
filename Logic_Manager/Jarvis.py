import DatabaseManager
import facerecon as JarvisFaceRecognition

def main():
    db_manager = DatabaseManager.DatabaseManager()
    db_manager.retrieve_images(db_manager.get_drivers_list())  # getting known drivers images
    JarvisFaceRecognition.load_known_faces()  # loading the known faces
    driver = JarvisFaceRecognition.login()  # login using the face from the camera
    if driver:
        print("Welcome: " + driver)
        db_manager.update_driving_history(driver)
    else:
        print("driver not found!")
        db_manager.push_unknown_drivers()




if __name__ == "__main__":
    main()