import face_recognition
import os
import cv2
import PIL

KNOWN_FACES_DIR = 'knownDrivers'
UNKNOWN_FACES_DIR = 'unknownDrivers'
TOLERANCE = 0.6
MODEL = 'hog'  # or cnn
global_known_faces = []
global_known_names = []


def main():
    load_known_faces()
    driver = login()
    # if driver found, returning his name, else returning false
    if driver:
        print("Welcome: " + driver)
    else:
        user_choice = input("driver not found!")


def login():
    for i in range(3):
        capture_face(UNKNOWN_FACES_DIR)
        identification = identify_person()
        print("identification: " + identification)
        if(identification != "Not found!"):
            return identification
    return False


def capture_face(path):
    cascPath = "haarcascade_frontalface_default.xml"
    faceCascade = cv2.CascadeClassifier(cascPath)
    video_capture = cv2.VideoCapture(0)
    while True:
        # Capture frame-by-frame
        ret, frame = video_capture.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30),
        )
        counter = 0
        for (x, y, w, h) in faces:
            counter = counter + 1
        if(counter == 1):  # if theres ine face... because we want to picture only the driver
            cv2.imwrite(path + "/" + "driver.png", frame)
            return True
            print("Face detected!")


def load_known_faces():
    print('Loading known faces...')
    for name in os.listdir(KNOWN_FACES_DIR):
        # Next we load every file of faces of known person
        for filename in os.listdir(KNOWN_FACES_DIR + '/' + name):
            # Load an image
            image = face_recognition.load_image_file(KNOWN_FACES_DIR + '/' + name + '/' + filename)
            # Get 128-dimension face encoding
            # Always returns a list of found faces, for this purpose we take first face only (assuming one face per image as you can't be twice on one image)
            encoding = face_recognition.face_encodings(image)
            if len(encoding) == 1:
                # Append encodings and name
                global_known_faces.append(encoding)
                global_known_names.append(name)


def identify_person():

    print('identifying driver...(Registered: ' + str(len(global_known_faces)) + ' drivers)')
    # looping on folder with faces ti identify
    for filename in os.listdir(UNKNOWN_FACES_DIR):
        # Load image
        image = face_recognition.load_image_file(UNKNOWN_FACES_DIR + '/' + filename)

         # This time we first grab face locations - we'll need them to draw boxes
        locations = face_recognition.face_locations(image, model=MODEL)
        match = "Not found!"
        unknown_encoding = face_recognition.face_encodings(image, locations)
        if len(unknown_encoding) != 0:
            unknown_encoding = unknown_encoding[0]

            for face_encoding, name in zip(global_known_faces, global_known_names):
                encoding_match = face_recognition.compare_faces(unknown_encoding, face_encoding, tolerance=TOLERANCE)
                if encoding_match[0]:
                    match = name
        return match


if __name__ == "__main__":
    main()
