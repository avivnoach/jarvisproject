package com.example.jarvisclient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class
DrivingInsights extends AppCompatActivity {

    DatabaseReference databaseRef;
    ImageView backBtn, circleBackground;
    TextView driverScore;
    final ArrayList<User> connectedUser = new ArrayList<User>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driving_insights);
        driverScore = (TextView)findViewById(R.id.score_text_view);
        backBtn = (ImageView)findViewById(R.id.close_insights);
        circleBackground = (ImageView)findViewById(R.id.colored_circle);
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

        Query query = reference.child("Users").orderByChild("uid").equalTo(FirebaseAuth.getInstance().getUid());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot user : dataSnapshot.getChildren()) {
                        Insights res = new Insights();
                        connectedUser.add(new User(
                                user.child("username").getValue().toString(),
                                user.child("email").getValue().toString(),
                                user.child("name").getValue().toString(),
                                user.child("uid").getValue().toString()));
                        getInsights(connectedUser.get(0).getUsername(), res);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(DrivingInsights.this,"Error: couldn't read DataBase!",Toast.LENGTH_SHORT).show();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private Insights getInsights(final String username, final Insights resIns )
    {
        databaseRef = FirebaseDatabase.getInstance().getReference("Users").child(username).child("insights");
        databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    resIns.setTime_focus(Integer.parseInt(snapshot.child("time_focus").getValue().toString()));
                    resIns.setTime_unfocused(Integer.parseInt(snapshot.child("time_unfocused").getValue().toString()));
                    resIns.setUsername(username);
                    double sum = resIns.getTime_focus() + resIns.getTime_unfocused();
                    double focused = resIns.getTime_focus();
                    double score = (focused / sum) * 100;
                    if(score > 70)
                        circleBackground.setImageResource(R.drawable.great_driver);
                    else if (score > 40)
                        circleBackground.setImageResource(R.drawable.normal_driver);
                    else
                        circleBackground.setImageResource(R.drawable.risky_driver);
                    driverScore.setText(String.valueOf((int)Math.round(score)));
                }
                else
                {
                    Toast.makeText(DrivingInsights.this, "This user has no insights to show!", Toast.LENGTH_LONG).show();
                    driverScore.setText("!");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        return resIns;
    }
}