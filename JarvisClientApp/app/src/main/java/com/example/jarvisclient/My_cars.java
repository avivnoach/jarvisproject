package com.example.jarvisclient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class My_cars extends AppCompatActivity {

    ImageView addCarButton, backButton;
    ProgressBar pb;
    final ArrayList<User> connectedUser = new ArrayList<User>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cars);
        addCarButton = findViewById(R.id.add_car_button);
        backButton = findViewById(R.id.back_to_dashboard_button);
        pb = (ProgressBar)findViewById(R.id.myCarsProgressBar);
        ArrayList<Car> cars = new ArrayList<Car>();
        CarsAdapter adapter = null;
        final RecyclerView rvCars = (RecyclerView)findViewById(R.id.rv_cars);
        final ArrayList<String> carsNumbers = new ArrayList<String>();
        final ArrayList<Car> finalCarsList = new ArrayList<Car>();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        Query query = reference.child("Users").orderByChild("uid").equalTo(FirebaseAuth.getInstance().getUid());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot user : dataSnapshot.getChildren()) {
                        connectedUser.add(new User(
                                user.child("username").getValue().toString(),
                                user.child("email").getValue().toString(),
                                user.child("name").getValue().toString(),
                                user.child("uid").getValue().toString()));
                    }

                    DatabaseReference firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(connectedUser.get(0).getUsername()).child("driving");
                    firebaseDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for(DataSnapshot number : dataSnapshot.getChildren())
                            {
                                carsNumbers.add(number.getValue().toString());
                            }
                            for(final String carNumber : carsNumbers)
                            {
                                FirebaseDatabase.getInstance().getReference().child("Cars").child(carNumber)
                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    ArrayList<String> tempDrivers = new ArrayList<String>();
                                                    for (DataSnapshot driverUsername : dataSnapshot.child("drivers").getChildren()) {
                                                        tempDrivers.add(driverUsername.getValue().toString());
                                                    }
                                                    finalCarsList.add(
                                                            new Car(
                                                                    dataSnapshot.child("name").getValue().toString(),
                                                                    Integer.parseInt(dataSnapshot.child("number").getValue().toString()),
                                                                    dataSnapshot.child("ownerUsername").getValue().toString(),
                                                                    tempDrivers));
                                                    //Create adapter passing in the sample user data
                                                    final CarsAdapter adapter = new CarsAdapter(finalCarsList);
                                                    // Attach the adapter to the recyclerview to populate items
                                                    rvCars.setAdapter(adapter);
                                                    // Set layout manager to position the items
                                                    rvCars.setLayoutManager(new LinearLayoutManager(My_cars.this));
                                                    pb.setVisibility(View.GONE);
                                                }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                            }
                                        });
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });

                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(My_cars.this,"Error: couldn't read DataBase!",Toast.LENGTH_SHORT).show();
            }
        });


        addCarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addCar = new Intent(My_cars.this, New_car.class);
                startActivity(addCar);
                finish();
            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



    }
}