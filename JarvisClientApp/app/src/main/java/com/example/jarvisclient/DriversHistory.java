package com.example.jarvisclient;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class DriversHistory extends AppCompatActivity {

    DatabaseReference databaseRef;
    final ArrayList<User> connectedUser = new ArrayList<User>();
    ImageView backButton;
    ProgressBar pb;
    NumberPicker picker;
    TextView errorText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drivers_history);
        picker = (NumberPicker)findViewById(R.id.car_number_picker);
        backButton = findViewById(R.id.close_drivers_history);
        pb = (ProgressBar)findViewById(R.id.history_progress_bar);
        errorText = (TextView)findViewById(R.id.error_text);
        ArrayList<Car> cars = new ArrayList<Car>();
        CarsAdapter adapter = null;
        final RecyclerView rvHistory = (RecyclerView)findViewById(R.id.rv_history);
        final ArrayList<String> carsNumbers = new ArrayList<String>();
        final ArrayList<History> finalHistoryList = new ArrayList<History>();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        Query query = reference.child("Users").orderByChild("uid").equalTo(FirebaseAuth.getInstance().getUid());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot user : dataSnapshot.getChildren()) {
                        connectedUser.add(new User(
                                user.child("username").getValue().toString(),
                                user.child("email").getValue().toString(),
                                user.child("name").getValue().toString(),
                                user.child("uid").getValue().toString()));
                    }

                    DatabaseReference firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(connectedUser.get(0).getUsername()).child("driving");
                    firebaseDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for(DataSnapshot number : dataSnapshot.getChildren())
                            {
                                carsNumbers.add(number.getValue().toString());

                            }
                            String[] carsArr = new String[carsNumbers.size()];
                            carsArr = carsNumbers.toArray(carsArr);
                            picker.setMaxValue(carsNumbers.size()-1);
                            picker.setMinValue(0);
                            picker.setDisplayedValues(carsArr);
                            finalHistoryList.clear();
                            pb.setVisibility(View.VISIBLE);

                            FirebaseDatabase.getInstance().getReference().child("Cars").child(String.valueOf(carsNumbers.get(picker.getValue())))
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if(dataSnapshot.exists())
                                            {
                                                for (DataSnapshot day : dataSnapshot.child("Driving_history").getChildren()) {
                                                    for(DataSnapshot time : day.getChildren())
                                                    {
                                                        finalHistoryList.add(
                                                                new History(
                                                                        time.getValue().toString(),
                                                                        day.getKey(),
                                                                        time.getKey()));
                                                    }
                                                }
                                                if(finalHistoryList.size() == 0)
                                                {
                                                    errorText.setText("No history to show!");
                                                }
                                            }
                                            else
                                            {
                                                errorText.setText("You do not drive that car!");
                                            }
                                            errorText.setText("");
                                            //Create adapter passing in the sample user data
                                            final HistoryAdapter adapter = new HistoryAdapter(finalHistoryList);
                                            // Attach the adapter to the recyclerview to populate items
                                            rvHistory.setAdapter(adapter);
                                            // Set layout manager to position the items
                                            rvHistory.setLayoutManager(new LinearLayoutManager(DriversHistory.this));
                                            pb.setVisibility(View.GONE);
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                        }
                                    });
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });

                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(DriversHistory.this,"Error: couldn't read DataBase!",Toast.LENGTH_SHORT).show();
            }
        });

        picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                finalHistoryList.clear();
                pb.setVisibility(View.VISIBLE);

                FirebaseDatabase.getInstance().getReference().child("Cars").child(String.valueOf(carsNumbers.get(picker.getValue())))
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists())
                                {
                                    for (DataSnapshot day : dataSnapshot.child("Driving_history").getChildren()) {
                                        for(DataSnapshot time : day.getChildren())
                                        {
                                            finalHistoryList.add(
                                                    new History(
                                                            time.getValue().toString(),
                                                            day.getKey(),
                                                            time.getKey()));
                                        }
                                    }
                                    if(finalHistoryList.size() == 0)
                                    {
                                        errorText.setText("No history to show!");
                                    }
                                }
                                else
                                {
                                    errorText.setText("You do not drive that car!");
                                }
                                errorText.setText("");
                                //Create adapter passing in the sample user data
                                final HistoryAdapter adapter = new HistoryAdapter(finalHistoryList);
                                // Attach the adapter to the recyclerview to populate items
                                rvHistory.setAdapter(adapter);
                                // Set layout manager to position the items
                                rvHistory.setLayoutManager(new LinearLayoutManager(DriversHistory.this));
                                pb.setVisibility(View.GONE);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                        });
                }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

}