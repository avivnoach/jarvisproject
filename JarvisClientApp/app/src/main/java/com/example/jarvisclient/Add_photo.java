package com.example.jarvisclient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Add_photo extends AppCompatActivity {
    int PHOTO_FROM_GALLERY = 1548;
    int PHOTO_FROM_CAMERA = 1910;
    Uri imageUri;
    File photoFile = null;
    String mCurrentPhotoPath;
    String username;
    boolean addedPhoto = false;
    ImageView fromCamera, fromGallery;
    ImageView finishSignupButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_photo);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        fromCamera = (ImageView)findViewById(R.id.take_photo_btn);
        fromGallery = (ImageView)findViewById(R.id.choose_photo_from_gallery_btn);
        finishSignupButton = (ImageView)findViewById(R.id.finish_sign_up_btn);
        username = getIntent().getExtras().getString("username");
        fromCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                imageUri = getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(intent, PHOTO_FROM_CAMERA);
            }
        });
        fromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PHOTO_FROM_GALLERY);
            }
        });
        finishSignupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addedPhoto)
                {
                    Intent toDashboard = new Intent();
                    startActivity(toDashboard);
                    finish();
                }
                else
                {
                    Toast.makeText(Add_photo.this, "Please upload a photo of yourself to continue", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    private void upload_pic (Uri uri) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Uploading...");
        progressDialog.show();
        final StorageReference ref = FirebaseStorage.getInstance().getReference()
                .child("DriversPictures").child(username + ".jpeg");
        ref.putFile(uri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        progressDialog.dismiss();
                        Toast.makeText(Add_photo.this, "Uploaded", Toast.LENGTH_SHORT).show();
                        addedPhoto = true;
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.dismiss();
                        Toast.makeText(Add_photo.this, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                .getTotalByteCount());
                        progressDialog.setMessage("Uploaded "+(int)progress+"%");
                    }
                });
    }


    // function gets the url for downloading the photo
    private void getDownloadUrl(StorageReference store_ref)
    {
        store_ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                updateUserProfilePicOnStorage(uri);
            }
        });
    }

    // function updates the profile picture in the firebase storage
    private void updateUserProfilePicOnStorage(Uri uri)
    {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        UserProfileChangeRequest request = new UserProfileChangeRequest.Builder().setPhotoUri(uri).build();
        user.updateProfile(request).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(Add_photo.this, "Profile picture updated!", Toast.LENGTH_SHORT).show();
                addedPhoto = true;
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(Add_photo.this, "Failed to update profile picture!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PHOTO_FROM_CAMERA)
        {
            switch (resultCode)
            {
                case RESULT_OK:
                    upload_pic(imageUri);
                    break;
                default:
                    Toast.makeText(Add_photo.this, "Profile Picture Update Failed!", Toast.LENGTH_SHORT).show();
                    break;
            }

        }
        else if (requestCode == PHOTO_FROM_GALLERY && data.getData() != null)
        {
            switch (resultCode)
            {
                case RESULT_OK:
                    upload_pic(data.getData());
                    //upload_pic(selectedImage);
                    break;
                    default:
                    Toast.makeText(Add_photo.this, "Profile Picture Update Failed!", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String storageDir = Environment.getExternalStorageDirectory() + "/picupload";
        File dir = new File(storageDir);
        if (!dir.exists())
            dir.mkdir();
        File image = new File(storageDir + "/" + username + ".jpeg");
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.e("photo path = " , mCurrentPhotoPath);
        return image;
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

}