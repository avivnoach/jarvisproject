package com.example.jarvisclient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.sql.Driver;
import java.util.ArrayList;

public class Dashboard extends AppCompatActivity {
    ImageView aiGif;
    Button driversHistory, myCars, drivingInsights, manageDrivers, logoutBtn;
    FirebaseAuth mAuth = FirebaseAuth.getInstance();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.dashboard_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case R.id.aboutApp:
                Intent openAppInfo = new Intent(Dashboard.this, AppInfo.class);
                startActivity(openAppInfo);
                break;

            case R.id.aboutDeveloper:
                Intent openDeveloperInfo = new Intent(Dashboard.this, DeveloperInfo.class);
                startActivity(openDeveloperInfo);
                break;
                
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        logoutBtn = findViewById(R.id.logout_button);
        driversHistory = findViewById(R.id.drivers_history_button);
        drivingInsights = findViewById(R.id.driving_insights_button);
        myCars = findViewById(R.id.my_cars_button);
        manageDrivers = findViewById(R.id.manag_cars_button);

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                finish();
                onBackPressed();
            }
        });

        myCars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toMyCars = new Intent(Dashboard.this, My_cars.class);
                startActivity(toMyCars);
            }
        });

        driversHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toHistory = new Intent(Dashboard.this, DriversHistory.class);
                startActivity(toHistory);
            }
        });

        drivingInsights.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toInsights = new Intent(Dashboard.this, DrivingInsights.class);
                startActivity(toInsights);
            }
        });
        manageDrivers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toManage = new Intent(Dashboard.this, ManageDrivers.class);
                startActivity(toManage);
            }
        });


    }



}