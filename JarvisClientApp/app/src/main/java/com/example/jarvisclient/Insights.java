package com.example.jarvisclient;

public class Insights {
    private String username;
    private int time_focus;
    private int time_unfocused;

    public Insights(String username, int time_focus, int time_unfocused)
    {
        this.username = username;
        this.time_focus = time_focus;
        this.time_unfocused = time_unfocused;
    }

    public Insights()
    {
        this.username = "";
        this.time_focus = 0;
        this.time_unfocused = 0;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getTime_focus() {
        return time_focus;
    }

    public void setTime_focus(int time_focus) {
        this.time_focus = time_focus;
    }

    public int getTime_unfocused() {
        return time_unfocused;
    }

    public void setTime_unfocused(int time_unfocused) {
        this.time_unfocused = time_unfocused;
    }

    @Override
    public String toString() {
        return "Insights{" +
                "username='" + username + '\'' +
                ", time_focus=" + String.valueOf(time_focus) +
                ", time_unfocused=" + String.valueOf(time_unfocused) +
                '}';
    }
}
