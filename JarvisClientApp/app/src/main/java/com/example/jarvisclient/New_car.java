package com.example.jarvisclient;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class New_car extends AppCompatActivity {
    ArrayList<Pair<String, String>> drivers = new ArrayList<Pair<String, String>>(5); // pair: username, full name
    ImageView backButton;
    boolean[] occupiedDriver = {false, false, false, false, false};
    EditText[] driverBlocks = new EditText[4];
    Button[] addDriverButtons = new Button[4];
    ImageView addCarButton;
    EditText carName, carNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_car);
        carName = (EditText)findViewById(R.id.car_name_edit_text);
        carNumber = (EditText)findViewById(R.id.car_number_edit_text);
        addCarButton = (ImageView)findViewById(R.id.create_car_btn);
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        Query query = reference.child("Users").orderByChild("uid").equalTo(FirebaseAuth.getInstance().getUid());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        drivers.add(new Pair(issue.child("username").getValue().toString(),issue.child("name").getValue().toString()));
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(New_car.this,"Error: couldn't read DataBase!",Toast.LENGTH_SHORT).show();
            }
        });
        backButton = findViewById(R.id.back_to_my_cars_button);
        driverBlocks[0] = findViewById(R.id.driver1_block);
        driverBlocks[1] = findViewById(R.id.driver2_block);
        driverBlocks[2] = findViewById(R.id.driver3_block);
        driverBlocks[3] = findViewById(R.id.driver4_block);
        addDriverButtons[0] = findViewById(R.id.add_driver1_btn);
        addDriverButtons[1] = findViewById(R.id.add_driver2_btn);
        addDriverButtons[2] = findViewById(R.id.add_driver3_btn);
        addDriverButtons[3] = findViewById(R.id.add_driver4_btn);


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder
                        = new AlertDialog
                        .Builder(New_car.this);

                builder.setMessage("Discard changes?");

                builder.setTitle("Alert!");


                builder.setCancelable(false);


                builder
                        .setPositiveButton(
                                "Yes",
                                new DialogInterface
                                        .OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which)
                                    {
                                        finish();
                                    }
                                });
                builder
                        .setNegativeButton(
                                "No",
                                new DialogInterface
                                        .OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which)
                                    {


                                        dialog.cancel();
                                    }
                                });

                // Create the Alert dialog
                AlertDialog alertDialog = builder.create();

                // Show the Alert Dialog box
                alertDialog.show();
            }
        });

        addDriverButtons[0].setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                String driverUsername = driverBlocks[0].getText().toString();
                if(driverUsername.isEmpty()){
                    driverBlocks[0].setError("Enter driver's username");
                    driverBlocks[0].requestFocus();
                }
                else
                {
                   drivers = updateBlock(driverBlocks[0].getText().toString(), 0, drivers);
                }
            }
        });
        addDriverButtons[1].setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                String driverUsername = driverBlocks[1].getText().toString();
                if(driverUsername.isEmpty()){
                    driverBlocks[1].setError("Enter driver's username");
                    driverBlocks[1].requestFocus();
                }
                else
                {
                    drivers = updateBlock(driverBlocks[1].getText().toString(), 1, drivers);
                }
            }
        });
        addDriverButtons[2].setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                String driverUsername = driverBlocks[2].getText().toString();
                if(driverUsername.isEmpty()){
                    driverBlocks[2].setError("Enter driver's username");
                    driverBlocks[2].requestFocus();
                }
                else
                {
                    drivers = updateBlock(driverBlocks[2].getText().toString(),2 , drivers);
                }
            }
        });
        addDriverButtons[3].setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                String driverUsername = driverBlocks[3].getText().toString();
                if(driverUsername.isEmpty()){
                    driverBlocks[3].setError("Enter driver's username");
                    driverBlocks[3].requestFocus();
                }
                else
                {
                    drivers = updateBlock(driverBlocks[3].getText().toString(), 3, drivers);
                }
            }
        });
        addCarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Car DBcar = new Car(carName.getText().toString(), Integer.parseInt(carNumber.getText().toString()), drivers.get(0).first, removeDriversNameFromList(drivers));
                DBcar.pushCarToDB(DBcar); // pushing the car to the database
                updateUsersOnNewCar(carNumber.getText().toString(),drivers);
                Intent backToMyCars = new Intent(New_car.this, My_cars.class);
                startActivity(backToMyCars);
                finish();
            }
        });


    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private ArrayList<Pair<String, String>> updateBlock(final String username, final int blockIndex, final ArrayList<Pair<String, String>> drivers)
    {
        if(occupiedDriver[blockIndex] == true)
        {
            for (Pair p : drivers)
                if(p.second.equals(driverBlocks[blockIndex].getText().toString()))
                    drivers.remove(p);
            driverBlocks[blockIndex].setText("");
            driverBlocks[blockIndex].setBackground(getDrawable(R.drawable.add_driver_block));
            driverBlocks[blockIndex].setEnabled(true);
            addDriverButtons[blockIndex].bringToFront();
            occupiedDriver[blockIndex] = false;
        }
        else
        {
            try
            {
                DatabaseReference firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(username);
                firebaseDatabase.addValueEventListener(new ValueEventListener() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.child("name").getValue() != null)
                        {
                            drivers.add(new Pair<String, String>(username, dataSnapshot.child("name").getValue().toString()));
                            driverBlocks[blockIndex].setText(dataSnapshot.child("name").getValue().toString());
                            driverBlocks[blockIndex].setBackground(getDrawable(R.drawable.remove_driver_block));
                            driverBlocks[blockIndex].setEnabled(false);
                            addDriverButtons[blockIndex].bringToFront();
                            occupiedDriver[blockIndex] = true;
                        }
                        else
                        {
                            driverBlocks[blockIndex].setError("driver not found!");
                            driverBlocks[blockIndex].requestFocus();
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(New_car.this,"Error: couldn't read DataBase!",Toast.LENGTH_SHORT).show();
                    }
                });
            }
            catch (Exception c)
            {
                Toast.makeText(New_car.this,"Error",Toast.LENGTH_SHORT).show();
            }

        }
        return drivers;
    }

    private ArrayList<String> removeDriversNameFromList(ArrayList<Pair<String, String>> driversData)
    {
        ArrayList<String> driversUsernames = new ArrayList<String>();
        for(Pair<String, String> driver : driversData)
            driversUsernames.add(driver.first);
        return driversUsernames;
    }

    private void updateUsersOnNewCar(final String carNumber, ArrayList<Pair<String, String>> drivers)
    {
        final Map<String, String> driverCars = new HashMap<String, String>();
        for(final Pair<String , String> driver : drivers)
        {
            DatabaseReference firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(driver.first);
            firebaseDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    driverCars.clear();
                    if(snapshot.hasChild("driving"))
                    {
                        int counter = 0;
                        for(DataSnapshot i : snapshot.child("driving").getChildren())
                        {
                            driverCars.put(String.valueOf(counter), i.getValue().toString());
                            counter++;
                        }
                        driverCars.put(String.valueOf(counter), carNumber);
                    }
                    else
                    {
                        driverCars.put(String.valueOf(0), carNumber);
                    }
                    FirebaseDatabase.getInstance().getReference("Users").child(driver.first).child("driving").setValue(driverCars);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                }
            });
        }
    }

    public void onBackPressed()
    {
        AlertDialog.Builder builder
                = new AlertDialog
                .Builder(New_car.this);
        builder.setMessage("Discard changes?");
        builder.setTitle("Alert!");
        builder.setCancelable(false);
        builder
                .setPositiveButton(
                        "Yes",
                        new DialogInterface
                                .OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which)
                            {
                                finish();
                            }
                        });
        builder
                .setNegativeButton(
                        "No",
                        new DialogInterface
                                .OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which)
                            {
                                dialog.cancel();
                            }
                        });

        // Create the Alert dialog
        AlertDialog alertDialog = builder.create();

        // Show the Alert Dialog box
        alertDialog.show();
    }

}