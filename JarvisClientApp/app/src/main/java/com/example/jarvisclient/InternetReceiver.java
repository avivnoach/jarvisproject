package com.example.jarvisclient;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class InternetReceiver extends BroadcastReceiver {
    boolean noConnection;
    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        boolean connected = wifi != null && wifi.isConnectedOrConnecting()
                || mobile != null && mobile.isConnectedOrConnecting();

        if (!connected)
                Toast.makeText(context, "Not connected to Internet!", Toast.LENGTH_LONG);
        else
                Toast.makeText(context, "connected to Internet!", Toast.LENGTH_SHORT);

    }
}