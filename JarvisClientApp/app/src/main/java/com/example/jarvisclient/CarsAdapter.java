 package com.example.jarvisclient;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CarsAdapter extends RecyclerView.Adapter<CarsAdapter.ViewHolder>{

    private List<Car> mCars;
    private ImageView leaveCarBtn;
    final ArrayList<User> connectedUser = new ArrayList<User>();

    public CarsAdapter(List<Car> cars)
    {
        mCars = cars;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View userView = inflater.inflate(R.layout.car_card, parent, false);

        ViewHolder viewHolder = new ViewHolder(userView);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final Car car = mCars.get(position);
        TextView infoTV = holder.infoTextView;
        infoTV.setText(car.getName() + ", " + car.getNumber() + "\nOwner: " + car.getOwnerUsername());
        leaveCarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( connectedUser.size() == 0)
                {
                    DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                    Query query = reference.child("Users").orderByChild("uid").equalTo(FirebaseAuth.getInstance().getUid());
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                for (DataSnapshot user : dataSnapshot.getChildren()) {
                                    Insights res = new Insights();
                                    connectedUser.add(new User(
                                            user.child("username").getValue().toString(),
                                            user.child("email").getValue().toString(),
                                            user.child("name").getValue().toString(),
                                            user.child("uid").getValue().toString()));
                                    handleLeaving(String.valueOf(car.getNumber()), connectedUser.get(0).getUsername());
                                    mCars.remove(mCars.get(position));
                                    notifyItemRemoved(position);
                                }
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            // couldn't connect to database
                        }
                    });
                }
                else
                {
                    handleLeaving(String.valueOf(car.getNumber()), connectedUser.get(0).getUsername());
                }
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCars.size();
    }

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView infoTextView;
        public ImageView personImage;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            infoTextView = (TextView)itemView.findViewById(R.id.car_info);
            personImage = (ImageView)itemView.findViewById(R.id.car_icon);
            leaveCarBtn = (ImageView)itemView.findViewById(R.id.leave_car_icon);
        }
    }

    // function removes a user from a certain car, in case he chose to leave the car
    public void deleteDriverFromCar(final String driver, final String carNumber)
    {
        // deleting the car from the user allowed cars
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        DatabaseReference reference = database.child("Users").child(driver).child("driving");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot car : dataSnapshot.getChildren()) {
                    if(car.getValue().equals(carNumber))
                        car.getRef().removeValue();
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });

        // deleting the user from the car registered users
        reference = database.child("Cars").child(carNumber).child("drivers");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot tempDriver : dataSnapshot.getChildren()) {
                    if(tempDriver.getValue().equals(driver))
                        tempDriver.getRef().removeValue();
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
    }

    // function wipes a car from te database in case the owner chose to delete the car
    public void wipeCar(final String carNumber)
    {
        // deleting all users from the car
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        DatabaseReference reference = database.child("Cars").child(carNumber).child("drivers");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot tempDriver : dataSnapshot.getChildren()) {
                    deleteDriverFromCar(tempDriver.getValue().toString(), carNumber);
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });

        // deleting the car fromthe database
        reference = database.child("Cars").child(carNumber);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dataSnapshot.getRef().removeValue();
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
    }

    // function checks if the driver is the owner of the car
    // if he is - deleting the car if not - removing the driver from the car
    public void handleLeaving(final String carNumber, final String driver)
    {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        DatabaseReference reference = database.child("Cars").child(carNumber).child("ownerUsername");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue().toString().equals(driver))
                    wipeCar(carNumber);
                else
                    deleteDriverFromCar(driver, carNumber);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
    }


}
