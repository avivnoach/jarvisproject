package com.example.jarvisclient;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder>{

    private List<History> mHistoryList;

    public HistoryAdapter(List<History> historyList)
    {
        mHistoryList = historyList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View userView = inflater.inflate(R.layout.history_card, parent, false);

        ViewHolder viewHolder = new ViewHolder(userView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final History history = mHistoryList.get(position);
        TextView infoTV = holder.infoTextView;
        infoTV.setText(history.getDriverName() + ", " +  "\nDate: " + history.getDate() + ", " + history.getTime());
    }

    @Override
    public int getItemCount() {
        return mHistoryList.size();
    }

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView infoTextView;
        public ImageView personImage, removeBtn;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            infoTextView = (TextView)itemView.findViewById(R.id.history_info);
            personImage = (ImageView)itemView.findViewById(R.id.driver_icon);
            removeBtn = (ImageView)itemView.findViewById(R.id.remove_history_icon);
        }
    }

}
