package com.example.jarvisclient;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Iterator;

public class EditCar extends AppCompatActivity {
    ImageView backButton;
    boolean[] occupiedDriver = {false, false, false, false, false};
    EditText[] driverBlocks = new EditText[4];
    Button[] addDriverButtons = new Button[4];
    TextView carNumber;
    EditText carNameEditText;
    ImageView saveBtn, dontSaveBtn;
    ArrayList<Pair<String, String>> drivers = new ArrayList<Pair<String, String>>(); // pair: username, full name
    Pair<String, String> owner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_car);

        saveBtn = findViewById(R.id.saveBtn);
        dontSaveBtn = findViewById(R.id.dontSaveBtn);
        carNumber = findViewById(R.id.edit_car_number_text_view);
        carNameEditText = findViewById(R.id.edit_car_name_edit_text);
        driverBlocks[0] = findViewById(R.id.edit_driver1_block);
        driverBlocks[1] = findViewById(R.id.edit_driver2_block);
        driverBlocks[2] = findViewById(R.id.edit_driver3_block);
        driverBlocks[3] = findViewById(R.id.edit_driver4_block);
        addDriverButtons[0] = findViewById(R.id.edit_driver1_btn);
        addDriverButtons[1] = findViewById(R.id.edit_driver2_btn);
        addDriverButtons[2] = findViewById(R.id.edit_driver3_btn);
        addDriverButtons[3] = findViewById(R.id.edit_driver4_btn);

        final Car car = (Car) getIntent().getSerializableExtra("car");
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        final Query query = reference.child("Cars").orderByChild("number").equalTo(car.getNumber());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot issue : dataSnapshot.child(String.valueOf(car.getNumber())).child("drivers").getChildren()) {
                        getFullNameFromUsername(issue.getValue().toString(), car);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(EditCar.this,"Error: couldn't read DataBase!",Toast.LENGTH_SHORT).show();
            }
        });


        addDriverButtons[0].setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                String driverUsername = driverBlocks[0].getText().toString();
                if(driverUsername.isEmpty()){
                    driverBlocks[0].setError("Enter driver's username");
                    driverBlocks[0].requestFocus();
                }
                else
                {
                    drivers = updateBlock(driverBlocks[0].getText().toString(), 0, drivers);
                }
            }
        });
        addDriverButtons[1].setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                String driverUsername = driverBlocks[1].getText().toString();
                if(driverUsername.isEmpty()){
                    driverBlocks[1].setError("Enter driver's username");
                    driverBlocks[1].requestFocus();
                }
                else
                {
                    drivers = updateBlock(driverBlocks[1].getText().toString(), 1, drivers);
                }
            }
        });
        addDriverButtons[2].setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                String driverUsername = driverBlocks[2].getText().toString();
                if(driverUsername.isEmpty()){
                    driverBlocks[2].setError("Enter driver's username");
                    driverBlocks[2].requestFocus();
                }
                else
                {
                    drivers = updateBlock(driverBlocks[2].getText().toString(),2 , drivers);
                }
            }
        });
        addDriverButtons[3].setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                String driverUsername = driverBlocks[3].getText().toString();
                if(driverUsername.isEmpty()){
                    driverBlocks[3].setError("Enter driver's username");
                    driverBlocks[3].requestFocus();
                }
                else
                {
                    drivers = updateBlock(driverBlocks[3].getText().toString(), 3, drivers);
                }
            }
        });
        dontSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder
                        = new AlertDialog
                        .Builder(EditCar.this);
                builder.setMessage("Discard changes?");
                builder.setTitle("Alert!");
                builder.setCancelable(false);
                builder
                        .setPositiveButton(
                                "Yes",
                                new DialogInterface
                                        .OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which)
                                    {
                                        finish();
                                    }
                                });
                builder
                        .setNegativeButton(
                                "No",
                                new DialogInterface
                                        .OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which)
                                    {
                                        dialog.cancel();
                                    }
                                });

                // Create the Alert dialog
                AlertDialog alertDialog = builder.create();

                // Show the Alert Dialog box
                alertDialog.show();
            }
        });
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String finalCarName = carNameEditText.getText().toString();
                final Car finalCar = new Car(finalCarName, car.getNumber(), owner.first, removeDriversNameFromList(drivers));
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("Cars");
                Query query = mDatabase.child(String.valueOf(car.getNumber()));
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        snapshot.getRef().removeValue();
                        finalCar.pushCarToDB(finalCar);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Toast.makeText(EditCar.this,"Error: couldn't reach DataBase!",Toast.LENGTH_SHORT).show();
                    }
                });
                Intent backToManageDrivers = new Intent(EditCar.this, ManageDrivers.class);
                startActivity(backToManageDrivers);
                finish();
            }
        });

    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private ArrayList<Pair<String, String>> updateBlock(final String username, final int blockIndex, final ArrayList<Pair<String, String>> drivers)
    {
        ArrayList<Pair<String, String>> updatedDrivers = new ArrayList<Pair<String, String>>(drivers);
        if(occupiedDriver[blockIndex] == true)
        {
            for (Iterator<Pair<String, String>> it = updatedDrivers.iterator(); it.hasNext();)
            {
                if(((it.next().second.equals(driverBlocks[blockIndex].getText().toString()))))
                {
                    it.remove();
                    break;
                }
            }

            driverBlocks[blockIndex].setText("");
            driverBlocks[blockIndex].setBackground(getDrawable(R.drawable.add_driver_block));
            driverBlocks[blockIndex].setEnabled(true);
            addDriverButtons[blockIndex].bringToFront();
            occupiedDriver[blockIndex] = false;
        }
        else
        {
            try
            {
                DatabaseReference firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(username);
                firebaseDatabase.addValueEventListener(new ValueEventListener() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.child("name").getValue() != null)
                        {
                            drivers.add(new Pair<String, String>(username, dataSnapshot.child("name").getValue().toString()));
                            driverBlocks[blockIndex].setText(dataSnapshot.child("name").getValue().toString());
                            driverBlocks[blockIndex].setBackground(getDrawable(R.drawable.remove_driver_block));
                            driverBlocks[blockIndex].setEnabled(false);
                            addDriverButtons[blockIndex].bringToFront();
                            occupiedDriver[blockIndex] = true;
                        }
                        else
                        {
                            driverBlocks[blockIndex].setError("driver not found!");
                            driverBlocks[blockIndex].requestFocus();
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(EditCar.this,"Error: couldn't read DataBase!",Toast.LENGTH_SHORT).show();
                    }
                });
                updatedDrivers = drivers;
            }
            catch (Exception c)
            {
                Toast.makeText(EditCar.this,"Error",Toast.LENGTH_SHORT).show();
            }

        }
        return updatedDrivers;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void loadInfo(Car car)
    {
        carNameEditText.setText(car.getName());
        carNumber.setText("Car: " + car.getNumber());
        owner = new Pair<String, String>("", "");
        for(Pair<String, String> p : drivers)
        {
            if(p.first.equals(car.getOwnerUsername()))
                owner = p;
        }
        driverBlocks[0].setText(owner.second); // owner cannot be removed
        driverBlocks[0].setBackground(getDrawable(R.drawable.remove_driver_block));
        driverBlocks[0].setEnabled(false);
        addDriverButtons[0].setVisibility(View.INVISIBLE);
        occupiedDriver[0] = true;


        int i;
        for(i = 1; i < drivers.size(); i++);
        {
            if(drivers.get(i-1) != owner)
            {
                driverBlocks[i].setText(drivers.get(i-1).second); // setting full name
                driverBlocks[i].setBackground(getDrawable(R.drawable.remove_driver_block));
                driverBlocks[i].setEnabled(false);
                addDriverButtons[i].bringToFront();
                occupiedDriver[i] = true;
            }
        }
    }

    private void getFullNameFromUsername(final String username, final Car car)
    {
        final ArrayList<String> fullName = new ArrayList<String>();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        Query query = reference.child("Users").orderByChild("username").equalTo(username);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        drivers.add(new Pair<String, String>(username, issue.child("name").getValue().toString()));
                        loadInfo(car);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(EditCar.this,"Error: couldn't read DataBase!",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private ArrayList<String> removeDriversNameFromList(ArrayList<Pair<String, String>> driversData)
    {
        ArrayList<String> driversUsernames = new ArrayList<String>();
        for(Pair<String, String> driver : driversData)
            driversUsernames.add(driver.first);
        return driversUsernames;
    }

    public void onBackPressed()
    {
        AlertDialog.Builder builder
                = new AlertDialog
                .Builder(EditCar.this);
        builder.setMessage("Discard changes?");
        builder.setTitle("Alert!");
        builder.setCancelable(false);
        builder
                .setPositiveButton(
                        "Yes",
                        new DialogInterface
                                .OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which)
                            {
                                finish();
                            }
                        });
        builder
                .setNegativeButton(
                        "No",
                        new DialogInterface
                                .OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which)
                            {
                                dialog.cancel();
                            }
                        });

        // Create the Alert dialog
        AlertDialog alertDialog = builder.create();

        // Show the Alert Dialog box
        alertDialog.show();
    }
}