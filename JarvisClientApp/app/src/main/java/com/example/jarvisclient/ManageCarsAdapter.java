 package com.example.jarvisclient;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

 public class ManageCarsAdapter extends RecyclerView.Adapter<ManageCarsAdapter.ViewHolder>{

     private List<Car> mCars;
     private ImageView editCarBtn;
     final ArrayList<User> connectedUser = new ArrayList<User>();

     public ManageCarsAdapter(List<Car> cars)
     {
         mCars = cars;
     }

     @NonNull
     @Override
     public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
         Context context = parent.getContext();
         LayoutInflater inflater = LayoutInflater.from(context);

         View userView = inflater.inflate(R.layout.car_manager_card, parent, false);

         ViewHolder viewHolder = new ViewHolder(userView);
         return viewHolder;

     }

     @Override
     public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
         final Car car = mCars.get(position);
         TextView infoTV = holder.infoTextView;
         infoTV.setText(car.getName() + ", " + car.getNumber() + "\nOwner: " + car.getOwnerUsername());
         editCarBtn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 final Intent editCarActivity = new Intent(v.getContext(), EditCar.class);
                 if( connectedUser.size() == 0)
                 {
                     DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                     Query query = reference.child("Users").orderByChild("uid").equalTo(FirebaseAuth.getInstance().getUid());
                     query.addListenerForSingleValueEvent(new ValueEventListener() {
                         @Override
                         public void onDataChange(DataSnapshot dataSnapshot) {
                             if (dataSnapshot.exists()) {
                                 for (DataSnapshot user : dataSnapshot.getChildren()) {
                                     Insights res = new Insights();
                                     connectedUser.add(new User(
                                             user.child("username").getValue().toString(),
                                             user.child("email").getValue().toString(),
                                             user.child("name").getValue().toString(),
                                             user.child("uid").getValue().toString()));
                                 }
                             }
                         }
                         @Override
                         public void onCancelled(DatabaseError databaseError) {
                             // couldn't connect to database
                         }
                     });
                 }
                 editCarActivity.putExtra("car", mCars.get(position));
                 editCarActivity.putExtra("car_number", car.getNumber());
                 editCarActivity.putExtra("owner_username", car.getOwnerUsername());
                 v.getContext().startActivity(editCarActivity);
             }
         });
     }

     @Override
     public int getItemCount() {
         return mCars.size();
     }

     // Provide a direct reference to each of the views within a data item
     // Used to cache the views within the item layout for fast access
     public class ViewHolder extends RecyclerView.ViewHolder {
         // Your holder should contain a member variable
         // for any view that will be set as you render a row
         public TextView infoTextView;
         public ImageView personImage;

         // We also create a constructor that accepts the entire item row
         // and does the view lookups to find each subview
         public ViewHolder(View itemView) {
             // Stores the itemView in a public final member variable that can be used
             // to access the context from any ViewHolder instance.
             super(itemView);

             infoTextView = (TextView)itemView.findViewById(R.id.car_info);
             personImage = (ImageView)itemView.findViewById(R.id.car_icon);
             editCarBtn = (ImageView)itemView.findViewById(R.id.edit_drivers_icon);
         }
     }

     // function removes a user from a certain car, in case he chose to leave the car
     public void deleteDriverFromCar(final String driver, final String carNumber)
     {
         // deleting the car from the user allowed cars
         DatabaseReference database = FirebaseDatabase.getInstance().getReference();
         DatabaseReference reference = database.child("Users").child(driver).child("driving");
         reference.addListenerForSingleValueEvent(new ValueEventListener() {
             @Override
             public void onDataChange(DataSnapshot dataSnapshot) {
                 for (DataSnapshot car : dataSnapshot.getChildren()) {
                     if(car.getValue().equals(carNumber))
                         car.getRef().removeValue();
                 }
             }
             @Override
             public void onCancelled(DatabaseError error) {
                 // Failed to read value
             }
         });

         // deleting the user from the car registered users
         reference = database.child("Cars").child(carNumber).child("drivers");
         reference.addListenerForSingleValueEvent(new ValueEventListener() {
             @Override
             public void onDataChange(DataSnapshot dataSnapshot) {
                 for (DataSnapshot tempDriver : dataSnapshot.getChildren()) {
                     if(tempDriver.getValue().equals(driver))
                         tempDriver.getRef().removeValue();
                 }
             }
             @Override
             public void onCancelled(DatabaseError error) {
                 // Failed to read value
             }
         });
     }
 }
