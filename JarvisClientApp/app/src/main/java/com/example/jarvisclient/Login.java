package com.example.jarvisclient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Login extends AppCompatActivity {


    TextView signupButton;
    InternetReceiver internetReceiver = new InternetReceiver();
    ImageView btn_login;
    EditText _email;
    EditText _pass;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;


    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_POWER_CONNECTED);
        filter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(internetReceiver, filter);
        mAuth.addAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(internetReceiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ActivityCompat.requestPermissions(this,new String[]{ Manifest.permission.ACCESS_NETWORK_STATE},1);

        mAuth = FirebaseAuth.getInstance();

        _email = (EditText)findViewById(R.id.email_text_box);
        _pass = (EditText)findViewById(R.id.password_text_box);
        btn_login = (ImageView) findViewById(R.id.login_button);

        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser mFirebaseUser = mAuth.getCurrentUser();
                //if( mFirebaseUser != null ){ // auto signing in after closing the app without signing out
                //    Toast.makeText(MainActivity.this,"You are logged in",Toast.LENGTH_SHORT).show();
                //    Intent i = new Intent(MainActivity.this, acitivity_dashboard.class);
                //    startActivity(i);
                //}
            }
        };






        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = _email.getText().toString();
                String pwd = _pass.getText().toString();
                if(email.isEmpty()){
                    _email.setError("Please enter email id");
                    _email.requestFocus();
                }
                else  if(pwd.isEmpty()){
                    _pass.setError("Please enter your password");
                    _pass.requestFocus();
                }
                else  if(email.isEmpty() && pwd.isEmpty()){
                    Toast.makeText(Login.this,"Fields Are Empty!",Toast.LENGTH_SHORT).show();
                }
                else  if(!(email.isEmpty() && pwd.isEmpty())){
                    mAuth.signInWithEmailAndPassword(email, pwd).addOnCompleteListener(Login.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(!task.isSuccessful()){
                                Toast.makeText(Login.this,"Login Error, Please Login Again",Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(Login.this,"Welcome...",Toast.LENGTH_SHORT).show();
                                Intent toDashboard = new Intent(Login.this,Dashboard.class);
                                startActivity(toDashboard);
                            }
                        }
                    });
                }

            }
        });


        signupButton = findViewById(R.id.sign_up_button);
        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, Signup.class);
                startActivity(intent);
            }
        });

    }

}