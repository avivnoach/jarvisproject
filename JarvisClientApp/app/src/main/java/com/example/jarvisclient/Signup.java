package com.example.jarvisclient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Signup extends AppCompatActivity {

    ImageView loginButton;

    FirebaseDatabase db;
    DatabaseReference myRef;

    EditText _email;
    EditText _fullname;
    EditText _pass;
    EditText _username;
    ImageView btn_signup;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        FirebaseApp.initializeApp(this);

        db = FirebaseDatabase.getInstance();
        myRef = db.getReference();

        mAuth = FirebaseAuth.getInstance();
        _username = (EditText)findViewById(R.id.username_text_box);
        _email = (EditText)findViewById(R.id.email_text_box);
        _fullname = (EditText)findViewById(R.id.fullname_text_box);
        _pass = (EditText)findViewById(R.id.password_text_box);
        btn_signup = (ImageView) findViewById(R.id.sign_up_button);
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = _email.getText().toString();
                final String pwd = _pass.getText().toString();
                final String fullname = _fullname.getText().toString();
                final String username = _username.getText().toString();
                if(fullname.isEmpty())
                {
                    _fullname.setError("Please enter full name");
                    _fullname.requestFocus();
                }
                if(email.isEmpty()){
                    _email.setError("Please enter email id");
                    _email.requestFocus();
                }
                if(pwd.isEmpty()){
                    _pass.setError("Please enter your password");
                    _pass.requestFocus();
                }
                switch(validUsername(username))
                {
                    case 1:
                    {
                        _username.setError("Please enter full name");
                        _username.requestFocus();
                        break;
                    }
                    case 2:
                    {
                        _username.setError("user name cannot contain .#$[] ");
                        _username.requestFocus();
                        break;
                    }
                    case 3:
                    {
                        _username.setError("user name already exists");
                        _username.requestFocus();
                        break;
                    }
                }

                if(!(email.isEmpty() && pwd.isEmpty() && validUsername(username) == 0 && fullname.isEmpty())){
                    mAuth.createUserWithEmailAndPassword(email, pwd).addOnCompleteListener(Signup.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(!task.isSuccessful()){
                                Toast.makeText(Signup.this,"Sign Up was unsuccessful, Please Try Again",Toast.LENGTH_SHORT).show();
                                Toast.makeText(Signup.this,task.getException().toString(),Toast.LENGTH_SHORT).show();
                            }
                            else{
                                    User user = new User (username, email, fullname, mAuth.getUid());
                                    user.addUserToDB(user);
                                    Intent finishSignup = new Intent(Signup.this,Add_photo.class);
                                    finishSignup.putExtra("username", username);
                                    startActivity(finishSignup);
                                    finish();
                                }
                        }
                    });
                }
                else{
                    Toast.makeText(Signup.this,"Error Occurred!",Toast.LENGTH_SHORT).show();

                }

            }
        });


        loginButton = findViewById(R.id.back_to_login_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private int validUsername(final String username)
    {
        final boolean[] usernameExists = {false};
        if(username.isEmpty())
            return 1;
        // '.', '#', '$', '[', or ']'
        else if(!(username.indexOf('.') == -1 ||
                username.indexOf('#') == -1 ||
                username.indexOf('$') == -1 ||
                username.indexOf('[') == -1 ||
                username.indexOf(']') == -1))
        {
            return 2;
        }
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference().child("Users");
        rootRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.hasChild(username)) {
                    usernameExists[0] = true;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(Signup.this,"Can noy access DB, try again later...",Toast.LENGTH_SHORT).show();
            }
        });
        if(usernameExists[0])
        {
            return 3;
        }
        return 0;
    }


    // [END on_start_check_user]
}
