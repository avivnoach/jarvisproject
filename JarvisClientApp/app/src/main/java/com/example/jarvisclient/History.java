package com.example.jarvisclient;

public class History {
    public History(String driverName, String date, String time) {
        this.driverName = driverName;
        this.date = date;
        this.time = time;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    private String driverName;
    private String date;
    private String time;
}
