package com.example.jarvisclient;

import android.widget.Adapter;
import android.widget.BaseAdapter;

import androidx.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Car implements Serializable {
    private String name;
    private int number;
    private String ownerUsername;
    private ArrayList<String> drivers;
    private String carIp;

    public Car(String name, int number, String ownerUsername, ArrayList<String> drivers, String carIp) {
        this.name = name;
        this.number = number;
        this.ownerUsername = ownerUsername;
        this.drivers = drivers;
        this.carIp = carIp;
    }

    public Car(String name, int number, String ownerUsername, ArrayList<String> drivers) {
        this.name = name;
        this.number = number;
        this.ownerUsername = ownerUsername;
        this.drivers = drivers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getOwnerUsername() {
        return ownerUsername;
    }

    public void setOwnerUsername(String ownerUsername) {
        this.ownerUsername = ownerUsername;
    }

    public ArrayList<String> getDrivers() {
        return drivers;
    }

    public void setDrivers(ArrayList<String> drivers) {
        this.drivers = drivers;
    }

    public String getCarIp() {
        return carIp;
    }

    public void setCarIp(String carIp) {
        this.carIp = carIp;
    }

    public void pushCarToDB(Car car)
    {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("Cars");
        mDatabase.child(String.valueOf(car.getNumber())).setValue(car);
    }

    public static ArrayList<Car> createCarListBelongsToUser(String username)
    {
        final ArrayList<String> carsNumbers = new ArrayList<String>();
        final ArrayList<Car> finalCarsList = new ArrayList<Car>();
        DatabaseReference firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(username).child("driving");
        firebaseDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot number : dataSnapshot.getChildren())
                {
                    carsNumbers.add(number.getValue().toString());
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        for(final String carNumber : carsNumbers)
        {
            firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("Cars").child(carNumber);
            firebaseDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for(DataSnapshot car : dataSnapshot.getChildren()) {
                        ArrayList<String> tempDrivers = new ArrayList<String>();
                        for (DataSnapshot driverUsername : car.child("drivers").getChildren()) {
                            tempDrivers.add(driverUsername.getValue().toString());
                        }
                        finalCarsList.add(
                                new Car(
                                        car.child("name").getValue().toString(),
                                        Integer.parseInt(car.child("number").getValue().toString()),
                                        car.child("ownerUsername").getValue().toString(),
                                        tempDrivers));
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });
        }

        return finalCarsList;
    }

}
