from helpers import *


def is_real_face(path):
    machine_learning = get_machine_learning_files(path)
    frame = get_frame()
    faces = get_faces(machine_learning["net"], frame)

    count_real = 0
    count_fake = 0

    for face in faces:
        if machine_learning["le"].classes_[numpy.argmax(machine_learning["model"].predict(face)[0])] == "real":
            count_real += 1
        else:
            count_fake += 1

    if count_real > count_fake:
        return True
    else:
        return False
