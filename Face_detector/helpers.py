from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
from imutils.video import VideoStream
from os import sep
import imutils
import pickle
import numpy
import time
import cv2


def get_machine_learning_files(path):
    """
    this function put all the files of the machine learning that need in a dict

    :path: the path to the folder with all the files
    :return: a dict with all the arguments which need for the machine learning
    """
    machine_learning_dict = {}

    proto_path = sep.join([path, "face_detector", "deploy.prototxt"])
    model_path = sep.join([path, "face_detector", "res10_300x300_ssd_iter_140000.caffemodel"])
    machine_learning_dict["net"] = cv2.dnn.readNetFromCaffe(proto_path, model_path)

    machine_learning_dict["model"] = load_model(sep.join([path, "liveness.model"]))
    machine_learning_dict["le"] = pickle.loads(open(sep.join([path, "le.pickle"]), "rb").read())

    return machine_learning_dict


def get_frame():
    vs = VideoStream(src=0).start()
    time.sleep(2.0)
    frame = vs.read()
    frame = imutils.resize(frame, width=600)

    return frame


def get_faces(net, frame):
    (height, width) = frame.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0))

    net.setInput(blob)
    detections = net.forward()

    faces = []

    for i in range(0, detections.shape[2]):
        confidence = detections[0, 0, i, 2]

        if confidence > 0.5:

            box = detections[0, 0, i, 3:7] * numpy.array([width, height, width, height])
            (start_x, start_y, end_x, end_y) = box.astype("int")

            start_x = max(0, start_x)
            start_y = max(0, start_y)
            end_x = min(width, end_x)
            end_y = min(height, end_y)

            face = frame[start_y:end_y, start_x:end_x]
            face = cv2.resize(face, (32, 32))
            face = face.astype("float") / 255.0
            face = img_to_array(face)
            face = numpy.expand_dims(face, axis=0)

            faces.append(face)
    return faces


def save_pic(name_of_pic):
  camera = cv2.VideoCapture(0)
  time.sleep(0.1)  # Wait for the camera to boot
  return_value, image = camera.read()
  cv2.imwrite(name_of_pic + ".png", image)
  del(camera) # free the camera
